package persistence;

import java.io.*;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/***
* Esta clase implementa un singletón cuya característica es que sólo es accesible mediantes sus métodos para escribir en un fichero
*/

public class SinSingletonStaticPI{

    private  FileWriter txtFile;
    private  PrintWriter txtPrintWriter;
    private   boolean isInitialized = false;
    private static long mFileCounter = 0;
    private static int mCounter =0;
        
    public SinSingletonStaticPI(long counter)throws FileNotFoundException {
    	mCounter += 1;
    	mFileCounter = counter;
    	System.out.println("Objeto no singleton: " + mCounter);
		open();
    }
		
    //s�lo lo vamnos a llamar desde los m�todos writeX, por lo que desde fuera, los �nicos m�todos putlicos ser�n los writeX. As�, tenemos un singlet�n que gestionamos desde dentro de la clase TraceWritter...a ver si no peta.
    
  
    
    //No tengo muy claro c�mo funciona el "isInitialized"...supongo que es para ver si existe el fichero verdad?
    private  void open()throws FileNotFoundException{
    	
    	if(!isInitialized) {
    		try {
    			//new File(dirFilename).mkdirs();
    			
			 txtFile = new FileWriter("./NoSingletonImplementation_file"+mFileCounter+".txt",true);
			//txtFilename = txtFile.getAbsolutePath();
    			isInitialized = true;
    		}
    		catch(Exception e) {
    			System.out.println("There is a problem while creating Log File " + e.toString() );
    		}
    	}
    	
    		/*
		try {
			if(!txtFile.createNewFile()) {
				//txtFile.delete();
				//txtFile.createNewFile();
			}else {
				//txtFile = new File(txtFilename);
				//txtFilename = txtFile.getAbsolutePath();
			}			
		}catch(Exception e) {
			System.out.println("Unable to create Log File " + e.toString() );
		}
		*/
		
			txtPrintWriter = new PrintWriter(txtFile);
    }
    
    private boolean isValidPath(String path) {
        try {
            Paths.get(path);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }


//Nuevas versiones de los m�todos write. Esto se sigue invicando desde fuera como TraceWritter.writeTxt("xsssssssss");
   /* public static void writeTxt(String text) throws Exception{
			//CONTAR NUMERO DE INSTANCIAS
    		singleInstance = new SingletonStaticPI();
			//AL CREAR LA NUEVA INSTANCIA, SE ABRE EL FICHERO, AS� NO HAY QUE ABRIR/CERRAR
			singleInstance.writeTxtInstance(text);
	    		    	
    }*/
    
    
    public  void writeTxt(String text) throws Exception{
    		
    		txtPrintWriter.append(text + "\r\n");
    		txtPrintWriter.flush();
    		closeFile();
    }
    
    
    
    private  void closeFile() throws Exception{
    		txtPrintWriter.close();
    }
}