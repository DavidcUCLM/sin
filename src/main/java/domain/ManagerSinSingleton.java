package domain;

import java.io.FileNotFoundException;

import persistence.SinSingletonStaticPI;

public class ManagerSinSingleton {
	
	private static long counter =  java.lang.System.currentTimeMillis();

	public static boolean throwClassicSingletonTest(String path, String recordText, int numRecord) {
		boolean sucess = false;
		
		return sucess;
	}
	
	public static void throwStaticAPISingletonTest(String path, String recordText, int numRecord) {
		boolean sucess = false;
		SinSingletonStaticPI  noSingleton = null;
		
		try {
		//	StatusObserver.notifyMsg("===============\n\tStarting writting "+numRecord+" records - "+System.currentTimeMillis());
			for(int i=0; i<numRecord; i++) {
				noSingleton = new SinSingletonStaticPI(counter);
				noSingleton.writeTxt(recordText+"_"+i);
			}
				
		//	StatusObserver.notifyMsg("\n\tFinishing writting "+numRecord+" records - "+System.currentTimeMillis()+"\n===============\n");
			counter+=1;
		}catch(FileNotFoundException fnfe) {
			System.err.println("Error en fichero:\n"+fnfe.toString());
		}catch(Exception e) {
			System.err.println("Error:\n"+e.toString());
		}
	}

	public static boolean throwFullStaticSingletonTest(String path, String recordText, int numRecord) {
		boolean sucess = false;
		
		return sucess;
	}

	
}
